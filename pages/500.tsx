import 'bootstrap/dist/css/bootstrap.min.css';
import styles from '../styles/Error.module.css';
import { Container } from 'react-bootstrap';
import Image from 'next/image'

const Custom500 = () => {
    return (
        <>
            <style jsx global>{`body { background-color: #feabb9; }`}</style>
            <Container className={styles.Error_robot}>
                <div className='row'>
                    <div className='col text-center'>
                        <Image src='/img/generic-error.gif' alt='Robot with error' width='600px' height='400px' />
                    </div>
                </div>
                <div className='row'>
                    <div className='col text-center'>
                        <h3 className='text-white mb-3'>Oops, something went wrong!</h3>
                        <h6 className='text-white mb-4'>Don't worry, everything is still awesome!</h6>
                        <a className='btn btn-secondary px-3' href='/'>
                            Back to Homepage {'>'}
                        </a>
                    </div>
                </div>
            </Container>
        </>
    );
};

export default Custom500;