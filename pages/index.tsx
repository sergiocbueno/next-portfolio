import 'bootstrap/dist/css/bootstrap.min.css';
import Head from 'next/head';
import Loading from '../components/Loading';
import Navigation from '../components/Navigation';
import Globe from '../components/Globe';
import AboutMe from '../components/AboutMe';
import Footer from '../components/Footer';
import fetch from 'isomorphic-unfetch';
import useSWR from 'swr';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import Experience from '../components/Experience';

const API_URL = `https://ipgeolocation.abstractapi.com/v1/?api_key=${process.env.NEXT_PUBLIC_ABSTRACT_API_KEY}`

async function fetcher(url) {
  const res = await fetch(url);
  const json = await res.json();
  return json;
}

const IndexPage = () => {
  const { data, error } = useSWR(API_URL, fetcher);
  const router = useRouter()

  useEffect(() => {
    if (error)
      router.push('/500');
  }, [error])

  if (!data) return <Loading loading={true} />;
  const { latitude, longitude, city, country } = data;

  return (
    <>
     <Head>
        {/* Basic Head Data */}
        <meta name='description' content='This is the portfolio of Sergio Bueno - Senior Software Engineer' />
        <meta name='author' content='Sergio Bueno' />
        <meta name='language' content='English' />
        <title>Sergio Bueno - Senior Software Engineer</title>
        <link rel='icon' href='/favicon.ico' />

        {/* Open Graph Data */}
        <meta property='og:title' content='Sergio Bueno - Senior Software Engineer' />
        <meta property='og:type' content='website' />
        <meta property='og:url' content='https://www.sergiobueno.me/' />
        <meta property='og:image' content='https://www.sergiobueno.me/img/me.jpg' />
        <meta property='og:description' content='This is the portfolio of Sergio Bueno - Senior Software Engineer' />

        {/* Twitter Card */}
        <meta name='twitter:card' content='summary' />
        <meta name='twitter:title' content='Sergio Bueno - Senior Software Engineer' />
        <meta name='twitter:description' content='This is the portfolio of Sergio Bueno - Senior Software Engineer' />
        <meta name='twitter:url' content='https://www.sergiobueno.me/' />
        <meta name='twitter:image' content='https://www.sergiobueno.me/img/me.jpg' />
     </Head>
     <Navigation />
     <Globe lat={latitude} lon={longitude} city={city} country={country} />
     <AboutMe />
     <Experience />
     <Footer />
    </>
  );
};
  
export default IndexPage