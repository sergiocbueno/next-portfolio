import { Navbar, Nav, Container } from 'react-bootstrap';

const Navigation = () => {
    return (
        <>
            <Navbar collapseOnSelect fixed='top' expand='sm' bg='transparent' variant='dark'>
                <Container>
                    <Navbar.Toggle aria-controls='responsive-navbar-nav' />
                    <Navbar.Collapse id='responsive-navbar-nav' className="justify-content-end">
                        <Nav>
                            <Nav.Link href='#home'>Home</Nav.Link>
                            <Nav.Link href='#about'>About</Nav.Link>
                            <Nav.Link href='#experience'>Experience</Nav.Link>
                            <Nav.Link href='#education'>Education</Nav.Link>
                            <Nav.Link href='#skills'>Skills</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
    );
};

export default Navigation;