import { VerticalTimeline, VerticalTimelineElement } from "react-vertical-timeline-component";
import 'react-vertical-timeline-component/style.min.css';
import { faKickstarter, faAws } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from '../styles/Experience.module.css';

const Experience = () => {
    return (
        <>
            <section id='experience'>
                <div className={`pb-5 ${styles.Experience_background}`}>
                    <div className='text-center pt-5 mb-5'>
                        <h4>EXPERIENCE</h4>
                    </div>
                    <VerticalTimeline>
                        <VerticalTimelineElement
                            className="vertical-timeline-element--work"
                            date="October 2020 ~ Present"
                            iconStyle={{
                                background: '#db6930',
                                color: '#fff'
                            }}
                            icon={
                                <FontAwesomeIcon icon={faAws} className={styles.Experience_main_icon}/>
                            }
                        >
                            <h4 className="vertical-timeline-element-title">Lead Software Engineer</h4>
                            <h5 className="vertical-timeline-element-subtitle">Cyklpoint Ltd - Attard, Malta</h5>
                            <p>
                                I have been working mainly as a backend developer in a multicultural team with a challenging context for a waste industry company. We have been developing an innovative waste cycle management digital platform.
                            </p>
                        </VerticalTimelineElement>
                        <VerticalTimelineElement
                            iconStyle={{
                                background: 'rgb(16, 204, 82)',
                                color: '#fff'
                            }}
                            icon={
                                <FontAwesomeIcon icon={faKickstarter} style={{width: "auto"}} />
                            }
                        />
                    </VerticalTimeline>
                </div>
            </section>
        </>
    );
};
    
export default Experience;