import React from 'react';
import styles from '../styles/Loading.module.css';

const Loading = (props: any) => {
  return (
    <div className={props.loading ? styles.Loader : styles.none}></div> 
  );
};

export default Loading;