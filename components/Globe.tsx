import styles from '../styles/Globe.module.css';
import dynamic from 'next/dynamic';
import { renderToStaticMarkup } from 'react-dom/server';
import Typed from 'react-typed';
const ReactGlobe = dynamic(import('react-globe.gl'), { ssr: false });

const _renderPopup = (location: string) => {
    return (
      <div className={styles.Connection}>
        <div className={styles.Connection_title}>Connection</div>
        <div className={styles.Connection_content}>
            From: {location}
            <br/>
            To: San Gwann, Malta
        </div>
      </div>
    );
};

function Globe({lat, lon, city, country}) {
    return (
        <>
            <section id='home'>
                <div className={styles.Globe_container}>
                    <div className={styles.Globe_connection}>
                        <ReactGlobe
                            globeImageUrl='//unpkg.com/three-globe/example/img/earth-night.jpg'
                            arcsData={[
                                {
                                    color: ['white', 'red'],
                                    endLat: lat,
                                    endLng: lon,
                                    name: renderToStaticMarkup(_renderPopup(city+', '+country)),
                                    startLat: 35.90847087650145,
                                    startLng: 14.48329675033368
                                } 
                            ]}
                            arcColor={'color'}
                            arcStroke={1.5}
                            arcDashLength={() => 1}
                            arcDashGap={() => 1}
                            arcDashAnimateTime={() => 4000}
                        />
                    </div>
                    <div className={styles.Globe_generic}>
                        <table className={styles.Globe_text}>
                            <tbody>
                            <tr>
                                <td>
                                    <strong>Portfolio of Sergio Bueno</strong>
                                </td>
                            </tr>
                            <tr>
                                <td className={styles.Globe_welcome_text}>
                                    <Typed
                                        strings={[
                                        `Welcome from ${country}!`
                                        ]}
                                        typeSpeed={80}
                                        startDelay={3000}
                                    />
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </>
    );
}

export default Globe;