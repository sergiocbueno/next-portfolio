import styles from '../styles/Footer.module.css';
import { faBitbucket, faFacebook, faLinkedin } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';

const Footer = () => {
    return (
        <>
            <footer className={`text-center text-lg-start ${styles.Footer_color}`}>
                <div className='text-center text-white pt-4'>
                    <h5>Keep in touch with me</h5>
                </div>
                <div className='container d-flex justify-content-center py-3'>
                    <a className={`mx-3 ${styles.Footer_icon}`} href='mailto:sergiocbueno@gmail.com' target='_blank'>
                        <FontAwesomeIcon icon={faEnvelope} />
                    </a>
                    <a className={`mx-3 ${styles.Footer_icon}`} href='https://www.linkedin.com/in/s%C3%A9rgio-da-costa-bueno-49a8ba13a/' target='_blank'>
                        <FontAwesomeIcon icon={faLinkedin} />
                    </a>
                    <a className={`mx-3 ${styles.Footer_icon}`} href='https://www.facebook.com/sergio.dacostabueno' target='_blank'>
                        <FontAwesomeIcon icon={faFacebook} />
                    </a>
                    <a className={`mx-3 ${styles.Footer_icon}`} href='https://bitbucket.org/sergiocbueno' target='_blank'>
                        <FontAwesomeIcon icon={faBitbucket} />
                    </a>
                </div>
                <div className={`text-center text-white p-3 ${styles.Footer_copyrights}`}>
                    Copyright © Sergio Bueno
                </div>
            </footer>
        </>
    );
};

export default Footer;