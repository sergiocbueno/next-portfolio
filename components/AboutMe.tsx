import styles from '../styles/About.module.css';
import { faCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Image from 'next/image'

const AboutMe = () => {
    return (
        <>
            <section id='about'>
                <div className='col-md-12'>
                    <div className='text-center mt-5 mb-5'>
                        <h4>ABOUT ME</h4>
                    </div>
                    <div className='row center mx-auto mb-5'>
                        <div className='col-md-4 mb-5 center'>
                            <div className={styles.About_polaroid}>
                                <div title='Sergio Bueno'>
                                    <Image src='/img/me.jpg' alt='Sergio Bueno Profile Picture' width='250px' height='250px' />
                                </div>
                            </div>
                        </div>
                        <div className='col-md-8 center'>
                            <div className='col-md-10'>
                                <div className='card'>
                                    <div className='card-header'>
                                        <span className='text-danger px-1'><FontAwesomeIcon icon={faCircle} /></span>
                                        <span className='text-warning px-1'><FontAwesomeIcon icon={faCircle} /></span>
                                        <span className='text-success px-1'><FontAwesomeIcon icon={faCircle} /></span>
                                    </div>
                                    <div className='card-body font-trebuchet text-justify ml-3 mr-3'>
                                        <p className='mb-5'><h5>Senior Software Engineer</h5></p>
                                        <p className='mb-3'>I am a Brazilian Software Engineer with 8+ years of experience, and currently, I am living in Malta.</p>
                                        <p className='mb-3'>Since 2014 I have been working with big clients around the world, with multidisciplinary and multicultural teams, and oriented by Agile methodologies.</p>
                                        <p className='mb-3'>I am capable to drive throughout the entire software life cycle, from requirement analysis to production support, including coding standards, code reviews, source control management, build processes, testing, deploying, and managing distributed applications on popular cloud vendors’ platforms.</p>
                                        <p className='mb-3'>My main characteristics are problem-solving, teamwork, proven leadership, creativity, and flexibility.</p>
                                        <p className='mb-3'>I am a big fan of the community concept, and for that reason, this application is totally open-source, you can access the <a className='text-decoration-none' href='https://bitbucket.org/sergiocbueno/myportfolio' target='_blank'><span className='text-primary'>source code</span></a> option and check it out.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
};

export default AboutMe;